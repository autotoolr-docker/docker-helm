FROM alpine:latest

LABEL maintainer="Autotoolr <https://gitlab.com/autotoolr-docker>" cli_version="3.3.1"

RUN apk -v --update add ca-certificates && \
    apk add --virtual=build curl tar zip gzip && \
    curl -LO https://get.helm.sh/helm-v3.3.1-linux-amd64.tar.gz && \
    tar -xzf helm-v3.3.1-linux-amd64.tar.gz --strip 1 && \
    chmod +x ./helm && \
    mv ./helm /usr/local/bin/helm && \
    apk --purge del build && \
    rm /var/cache/apk/*